# portfolio.py

# f = open('Data/portfolio.csv','r')
# for line in f:
#     print(line)
# f.close()



# total = 0.0

# with open('Data/portfolio.csv','r') as f:
#     headers = next(f)  # Skip first line
#     for line in f:
#         line = line.strip()    #Strip whitespace
#         parts = line.split(',')
#         parts[0] = parts[0].strip('"')
#         parts[1] = parts[1].strip('"')
#         parts[2] = int(parts[2])
#         parts[3] = float(parts[3])
#         total += parts[2]*parts[3]
# print('Total cost: ', total)

# Usage of CSV Library
# import csv
# total = 0.0

# with open('Data/portfolio2.csv','r') as f:
#     rows = csv.reader(f)
#     headers = next(rows)  # Skip first line
#     for row in rows:
#         row[2] = int(row[2])
#         row[3] = float(row[3])
#         total += row[2]*row[3]
# print('Total cost: ', total)

# Moving the script to a function

# def portfolio_cost(filename):
#     '''
#     Computes total shares*price for a CSV file with name, date, shares, price data
#     '''
#     total = 0.0
#     with open('Data/portfolio2.csv','r') as f:
#         rows = csv.reader(f)
#         headers = next(rows)  # Skip first line
#         for row in rows:
#             row[2] = int(row[2])
#             row[3] = float(row[3])
#             total += row[2]*row[3]
#     return total

# total = portfolio_cost('Data/portfolio.csv')
# print('Total cost: ', total)


# error handling e enumerate
import csv
def portfolio_cost(filename, *, errors='warn'):
    '''
    Computes total shares*price for a CSV file with name, date, shares, price data
    '''
    if errors not in {'warn', 'silent', 'raise'}:
        raise ValueError("errors must be one of 'warn', 'silent', 'raise'")
    total = 0.0
    with open('Data/portfolio2.csv','r') as f:
        rows = csv.reader(f)
        headers = next(rows)  # Skip first line
        for rowno, row in enumerate(rows, start=1):
            try:
                row[2] = int(row[2])
                row[3] = float(row[3])
            except ValueError as err:
                if errors == 'warn':
                    print('Row', rowno, 'Bad row', row)
                    print('Row', rowno, 'Reason', err)
                elif errors == 'raise':
                    raise  # Re raises last exception
            total += row[2]*row[3]
    return total

# total = portfolio_cost('Data/portfolio.csv')
# print('Total cost: ', total)

# Python Data Structures

# import csv

# from numpy import record
# def read_portfolio(filename, *, errors='warn'):
#     '''
#     Read a CSV file with name, date, shares, price data into a list
#     '''
#     if errors not in {'warn', 'silent', 'raise'}:
#         raise ValueError("erros must be one of 'warn', 'silent', 'raise'")

#     portfolio = []
#     with open('Data/portfolio2.csv','r') as f:
#         rows = csv.reader(f)
#         headers = next(rows)  # Skip first line
#         for rowno, row in enumerate(rows, start=1):
#             try:
#                 row[2] = int(row[2])
#                 row[3] = float(row[3])
#             except ValueError as err:
#                 if errors == 'warn':
#                     print('Row', rowno, 'Bad row', row)
#                     print('Row', rowno, 'Reason', err)
#                 elif errors == 'raise':
#                     raise  # Re raises last exception
#             # record=tuple(row)
#             record = {
#                 'name': row[0],
#                 'date': row[1],
#                 'shares': row[2],
#                 'price': row[3]
#             }
#             portfolio.append(record)
#     return portfolio

# portfolio  = read_portfolio('Data/portfolio.csv')
# print(portfolio)

# total_cost = sum([holding['shares']*holding['price'] for holding in portfolio])

# print('Total cost: ', total_cost)

#************************************
# Retrieve actual price and calculates profit/loss



def read_portfolio(filename, *, errorSpec='warn'):
    '''
    Read a CSV file with name, date, shares, price data into a list
    This function accepts two arguments: You should provide a filename with CSV
    And specify errorSpec with either 'warn', 'raise' or 'silent'
    So if it encounters a error, if errorSpec == 'warn' it will indicate the row and error type
    if errorSpec == 'raise', if it encounter an error it will raise in the stacktrace
    if errorSpec == 'silent', even if encounter an error, it won't print anything.
    '''
    if errorSpec not in {'warn', 'silent', 'raise'}:
        raise ValueError("errors must be one of 'warn', 'silent', 'raise'")

    portfolio = []
    with open(filename,'r') as f:
        rows = csv.reader(f)
        headers = next(rows)  # Skip first line
        for rowno, row in enumerate(rows, start=1):
            try:
                row[2] = int(row[2])
                row[3] = float(row[3])
                # record=tuple(row)
                record = {
                    'name': row[0],
                    'date': row[1],
                    'shares': row[2],
                    'price': row[3]
                }
                portfolio.append(record)
            except ValueError as err:
                if errorSpec == 'warn':
                    print('Row', rowno, 'Bad row', row)
                    print('Row', rowno, 'Reason', err)
                elif errorSpec == 'raise':
                    raise  # Re raises last exception
            else: pass # Do something if test is succesful
            finally: continue # do something at end and skips to the next row

    return portfolio


if __name__ == "__main__":
    print("read_portfolio can be ran standalone")
