# table.py
import sys
from abc import ABC, abstractmethod


def print_table_original(objects, colnames):
    """
    Make a nicely formatted table showing attributes from a list of objects
    :param objects:
    :param colnames: list with colunm fields to be displayed
    :return: a print of a formatted table
    """

    for colname in colnames:
        print('{:>10s}'.format(colname), end=' ')
    print()
    for obj in objects:
        for colname in colnames:
            print('{:>10s}'.format(str(getattr(obj, colname))), end=' ')
        print()


def print_table(objects, colnames, formatter):
    """
    Make a nicely formatted table showing attributes from a list of objects
    :param object: default object class
    :param colnames: list with colunm fields to be displayed
    :return: a print of a formatted table
    """

    # type checking

    if not isinstance(formatter, TableFormatter):
        raise TypeError('formatter must be a TableFormatter')

    formatter.headings(colnames)
    for obj in objects:
        rowdata = [str(getattr(obj, colname)) for colname in colnames]
        formatter.row(rowdata)


class TableFormatter(ABC):
    """
    Serves a design specification to format a table
    With variation to specify output (file or print screen)
    """

    def __init__(self, outfile=None):
        if outfile is None:
            outfile = sys.stdout
        self.outfile = outfile

    @abstractmethod
    def headings(self, headers):
        pass

    @abstractmethod
    def row(self, rowdata):
        pass


class TextTableFormatter(TableFormatter):
    """
    Implements a TableFormatter (by inheritance) and implements (overrides) its two methods
    Adds the outfile detail
    """

    def __init__(self, outfile=None, width=10): # include parent's param
        super().__init__(outfile)  # initialize parent
        self.width = width

    def headings(self, headers):
        for header in headers:
            print('{:>{}s}'.format(header, self.width), end=' ', file=self.outfile)
        print(file=self.outfile)

    def row(self, rowdata):
        for item in rowdata:
            print('{:>{}s}'.format(item, self.width), end=' ', file=self.outfile)
        print(file=self.outfile)


class CSVTableFormatter(TableFormatter):
    """
    Implements a TableFormatter (by inheritance) and implements (overrides) its two methods
    """

    def headings(self, headers):
        print(','.join(headers))

    def row(self, rowdata):
        print(','.join(rowdata))


class HTMLTableFormatter(TableFormatter):
    """
    Implements a TableFormatter (by inheritance) and implements (overrides) its two methods
    """

    def headings(self, headers):
        print('<tr>', end='')
        for h in headers:
            print('<th>{}</th>'.format(h), end='')
        print('</tr>')

    def row(self, rowdata):
        print('<tr>', end='')
        for d in rowdata:
            print('<td>{}</td>'.format(d), end='')
        print('</tr>')


class QuotedMixin:
    def row(self, rowdata):
        quoted = ['"{}"'.format(d) for d in rowdata]
        super().row(quoted)

if __name__ == '__main__':
    from holding import read_portfolio
    portfolio = read_portfolio('Data/portfolio.csv')
