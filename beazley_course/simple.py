# simple.py
""" Example of a module """

x = 42

def spam():
    print('x is ',x)

def run():
    print('Calling spam')
    spam()

if __name__ == '__main__':
    print('running ...')
    run()
