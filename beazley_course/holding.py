# holding.py

import csv


class Holding(object):
    """ Stock holding record: name, date, number of shares and purchased price """

    def __init__(self, name, date, shares, price):
        self.name = name
        self.date = date
        self.shares = shares
        self._price = price

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, newprice):
        if not isinstance(newprice, float):
            raise TypeError('Expected float')
        if newprice < 0:
            raise ValueError('Must be >0')
        self._price = newprice

    def __setattr__(self, key, value):
        if key not in {'name', 'date', 'shares', 'price', '_price'}:
            raise AttributeError(f'There is no attribute {key}')
        super().__setattr__(key, value)

    def __repr__(self):
        return 'Holding({}, {}, {}, {:.2f})'.format(self.name, self.date, self.shares, self.price)

    def __str__(self):
        return '{:6d} shares of {:4s} at {:4.2f}'.format(self.shares, self.name, self.price)

    def cost(self):
        return self.shares * self.shares

    def sell(self, nshares):
        self.shares -= nshares




class Portfolio:
    def __init__(self):
        self.holdings = []

    # By implementing this getattr, the Class portfolio acquires the methods of a list: append, sort, len, etc.
    def __getattr__(self, item):
        return getattr(self.holdings, item)

    @classmethod
    def from_csv(cls, filename):
        self = cls()  # cls is self initialized in __init__
        with open(filename, 'r') as f:
            rows = csv.reader(f)
            headers = next(rows)
            for row in rows:
                h = Holding(row[0], row[1], int(row[2]), float(row[3]))
                self.holdings.append(h)
        return self

    def __repr__(self):
        return str([p for p in self.holdings])

    def __len__(self):
        return len(self.holdings)

    def __iter__(self):
        return self.holdings.__iter__()

    def __getitem__(self, item):
        if isinstance(item, str):
            return [h for h in self.holdings if h.name == item]
        else:
            return self.holdings[item]

    def total_cost(self):
        return sum([p.shares * p.price for p in self.holdings])


def read_portfolio(filename):
    portfolio = []
    with open(filename, 'r') as f:
        rows = csv.reader(f)
        headers = next(rows)
        for row in rows:
            h = Holding(row[0], row[1], int(row[2]), float(row[3]))
            portfolio.append(h)
    return portfolio


if __name__ == '__main__':
    port = read_portfolio('Data/portfolio.csv')
