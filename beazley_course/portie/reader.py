# reader.py

import csv

def read_csv(filename, types, *, errorSpec='warn'):
    '''
    Read a CSV file with type conversion into a list of dicts
    '''
    if errorSpec not in {'warn', 'silent', 'raise'}:
        raise ValueError("errors must be one of 'warn', 'silent', 'raise'")

    records = []
    with open(filename,'r') as f:
        rows = csv.reader(f)
        headers = next(rows)  # Skip first line
        for rowno, row in enumerate(rows, start=1):
            try:
                row = [func(val) for func, val in zip(types, row)]
                record = dict(zip(headers, row))
                records.append(record)

            except ValueError as err:
                if errorSpec == 'warn':
                    print('Row', rowno, 'Bad row', row)
                    print('Row', rowno, 'Reason', err)
                elif errorSpec == 'raise':
                    raise  # Re raises last exception
            else: pass # Do something if test is succesful
            finally: continue # do something at end and skips to the next row

    return records


if __name__ == "__main__":
    print("read_csv can be ran standalone")
