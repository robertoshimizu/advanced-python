# port.py
import reader

def read_portfolio(filename, *, errorSpec='warn'):
    '''
    Read a portfolio of stocks
    '''

    return reader.read_csv(filename, [str, str, int, float], errorSpec=errorSpec)

if __name__ == "__main__":
    portfolio = read_portfolio('../Data/portfolio.csv')

    total = sum([holding['shares'] * holding['price'] for holding in portfolio])

    print('Total cost: ',total)
