# -*- coding: utf-8 -*-
"""
Spyder Editor

Este é um arquivo de script temporário.
"""

class Pessoa:
    olhos = 2
    def __init__(self, *filhos, nome=None, idade=35):
        self.nome = nome
        self.idade = idade
        self.filhos = list(filhos)
        
    def cumprimentar(self):
        return f'Olá {self.nome} {id(self)}'
    
    @staticmethod
    def metodo_estatico():
        return 'Método Classe'
    
    @classmethod
    def nome_atributos_classe(cls):
        return f'{cls} - olhos {cls.olhos}'
    
if __name__ == '__main__':
    renzo = Pessoa(nome='Renzo')
    luciano = Pessoa(renzo, nome='luciano')
    print(Pessoa.cumprimentar(renzo))
    print(Pessoa.cumprimentar(luciano))
    print(id(luciano))
    print(luciano.cumprimentar())
    
    # cria atributo de instancia durante execução
    luciano.sobrenome = 'Ramalho'
    print(luciano.__dict__)
    print(renzo.__dict__)
    
    #checa o atributo de classe
    #print(Pessoa.nome)
    print(Pessoa.olhos)
    print(renzo.olhos)
    print(id(Pessoa.olhos), id(renzo.olhos), id(luciano.olhos))
    print(Pessoa.metodo_estatico())
    print(id(Pessoa.metodo_estatico()), id(renzo.metodo_estatico()), id(luciano.metodo_estatico()))
    print(Pessoa.nome_atributos_classe())
    print(Pessoa.nome_atributos_classe(), renzo.nome_atributos_classe(), luciano.nome_atributos_classe())
    print(nome_atributos_classe(Pessoa))
    