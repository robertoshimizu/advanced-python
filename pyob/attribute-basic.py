# attribute-basic.py

# class coordinate:
#     '''coordinate on earth'''
#
#     # this are class variables/attributes
#
#     lat = 0.0
#     long = 0.0
#
#     def __repr__(self):
#         return f'coordinate({self.lat}, {self.long})'
#
#     def __str__(self):
#         ns = 'ns'[self.lat < 0]
#         we = 'ew'[self.long < 0]
#         return f'{abs(self.lat):.1f}°{ns}, {abs(self.long):.1f}°{we}'

from typing import NamedTuple, ClassVar

# class Coordinate(NamedTuple):
#     '''Coordinate on Earth'''
#
#     # This are class variables/attributes
#
#     lat: float = 0.0
#     long: float = 0.0
#
#     reference_system = 'WGS84'
#
#     def __repr__(self):
#         return f'Coordinate({self.lat}, {self.long})'
#
#     def __str__(self):
#         ns = 'NS'[self.lat < 0]
#         we = 'EW'[self.long < 0]
#         return f'{abs(self.lat):.1f}°{ns}, {abs(self.long):.1f}°{we}'

from dataclasses import dataclass

from typing import ClassVar


@dataclass
class Coordinate():
    '''Coordinate on Earth'''

    # This are class variables/attributes

    lat: float = 0.0
    long: float = 0.0

    reference_system: ClassVar[str] = 'WGS84'

    def __repr__(self):
        return f'Coordinate({self.lat}, {self.long})'

    def __str__(self):
        ns = 'NS'[self.lat < 0]
        we = 'EW'[self.long < 0]
        return f'{abs(self.lat):.1f}°{ns}, {abs(self.long):.1f}°{we}'


if __name__ == '__main__':
    for k, v in Coordinate.__dict__.items():
        if not k.startswith('_'):
            print(k, ':', v)
