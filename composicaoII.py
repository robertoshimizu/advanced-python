# composicaoII.py

class Stack:
    def __init__(self) -> list:
        self._items = list()
    
    def push(self, item):
        self._items.append(item)
    
    def pop(self):
        return self._items.pop()
    
    def __len__(self):
        return len(self._items)