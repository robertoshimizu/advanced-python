# Advanced Python

```Python
>>> import sys

>>> print(sys.version)
3.9.7 (default, May  8 2022, 19:35:41)
[Clang 13.0.0 (clang-1300.0.29.30)]
```

## Command Line usage, Scripts, f-strings

```Python
import sys

#print('Command options: ', sys.argv)
#raise SystemExit(0)

if len(sys.argv) != 3:
    raise SystemExit('Usage: nextbus.py route stopid')

route = sys.argv[1]
stopid = sys.argv[2]

import urllib.request

u = urllib.request.urlopen('http://ctabustracker.com/bustime/map/getStopPredictions.jsp?route={}&stop={}'.format(route, stopid))
data = u.read()

from xml.etree.ElementTree import XML
doc = XML(data)

import pdb; pdb.set_trace()  #Launch debugger

for pt in doc.findall('.//pt'):
    print(pt.text)
```

To run the code:

```bash
python3 nextbus.py 22 14787
```

Alternatively if you want to **run as a script**, add `#!/usr/bin/python3` at the top of the code and then type:

```bash
./nextbus.py 22 14787
```

If permission is denied, use this linux command `chmod +x nextbus.py`

### String formatting

```python
name = 'IBM'
shares = 100
price = 32.2

# %-formating
print('%10s %10d %10.2f', % (name, shares, price) )

# string.format()
print('{} {} {}'.format(name, shares, price))

# now specifying alignment > or < and digits
print('{:<10s} {:<10d} {:<10.2f}'.format(name, shares, price))

# You can reference variables in any order by referencing their index:
>>> "Hello, {1}. You are {0}.".format(age, name)
>>> 'Hello, Eric. You are 74.'

# You can also use ** to do this neat trick with dictionaries:
>>> person = {'name': 'Eric', 'age': 74}
>>> "Hello, {name}. You are {age}.".format(**person)
'Hello, Eric. You are 74.'
```

#### f-Strings: A New and Improved Way to Format Strings in Python

Also called “formatted string literals,” f-strings are string literals that have an f at the beginning and curly braces containing expressions that will be replaced with their values. The expressions are evaluated at runtime and then formatted using the `__format__ `protocol.

```python
name = 'IBM'
shares = 100
price = 32.2

# f-string
>>> print(f"{name:10s} {shares:10d} {price:10.2f}")
>>> 'IBM               100      32.20'
>>> f"Total value is {(shares * price):5.2f}"
>>> 'Total value is 3220.00'

user = {'name': 'John Doe', 'occupation': 'gardener'}
print(f"{user['name']} is a {user['occupation']}")
>>> John Doe is a gardener

y = 10
f'operação matematica {10*y=}'
'operação matematica 10*y=100'

```

## Debugging the code interactively using `python -i` option.

Example:
To run the code:

```bash
python3 -i nextbus.py 22 14787
```

It will run the program (even if it crashes) and then spin the interactive mode, so you can poke around.

### Debugger

Python has a debugger module called [pdb](https://docs.python.org/3/library/pdb.html).
If the program has crashed, you can enter in interactive mode and load the python debugger and then call the postmorten command, so it will show you the number line that crashed.

```python3
>>>import pdb
>>>pdb.pm()
```

You can launch the debugger in the code (see command at line), and then it will run and at the point it will run interectively. You can step trace.

```python
from xml.etree.ElementTree import XML
doc = XML(data)

import pdb; pdb.set_trace()  #Launch debugger at this line

for pt in doc.findall('.//pt'):
    print(pt.text)
```

## Write output to a File.

Below we open a file called `schedule.txt` and the we print with option `file=out`
Important do not forget to close the file using `out.close()` after program is executed.

```python
# mortgage.py
#
# Find out how long to pay off a mortgage

principal = 500000
payment = 2684.11
rate = 0.05
total_paid = 0

# Extra payment info
extra_payment = 1000
extra_payment_start_month = 1
extra_payment_end_month = 60
month = 0

out = open('schedule.txt','w')  # Open a file for writing

print('{:>5s} {:>10s} {:>10s} {:>10s}'.format('Month', 'Interest', 'Principal', 'Remaining'), file=out)
while principal > 0:
    month += 1
    if month >= extra_payment_start_month and month <= extra_payment_end_month:
        total_payment = payment + extra_payment
    else:
        total_payment = payment
    interest = principal * (rate/12)
    principal = principal + interest - total_payment
    total_paid += total_payment
    print('{:>5d} {:>10.2f} {:>10.2f} {:>10.2f}'.format(month, interest, total_payment - interest, principal), file=out)

out.close()
print('Total pago: ', total_paid)
```

### Manipulating Files

You can open a file, extract the data using this pattern:

```python
f = open('Data/portfolio.csv', 'r')
data = f.read()
for line in f:
  print(line)
f.close()
```

The more modern way is to using with `with` context, the advantage it will close the file after executing the block code:

```python
with open('Data/portfolio.csv', 'r') as f:
  data = f.read()
```

### Manipulating strings

Strings are arrays.

```python
line = '"IBM","2006-07-09",100,70.44\n'

>>> line[10]
'6'
>>> line[:10]
'"IBM","200'
>>> line[10:]
'6-07-09",100,70.44'
>>> line.strip()  #remove blank spaces including \n
'"IBM","2006-07-09",100,70.44'
>>> line.replace('"','-')
'-IBM-,-2006-07-09-,100,70.44\n'
>>> line.strip().split(',')
['"IBM"', '"2006-07-09"', '100', '70.44']
>>> parts = line.strip().split(',')
>>> parts[0]
'"IBM"'
>>> parts[0].strip('"')
'IBM'
# Convert other parts into numbers for further manipulation
>>> parts[0] = parts[0].strip('"')
>>> parts[1] = parts[1].strip('"')
>>> parts[2] = int(parts[2])
>>> parts[3] = float(parts[3])
>>> parts
['IBM', '2006-07-09', 100, 70.44]
>>> parts[2] * parts[3]
7044.0
```

### Functions, Error Handling and Modules

We may transform a script into a function.

#### Docstring

By default, the first string in the body of a method is used as its **docstring** (or documentation string). Python will use this when help() is called for that method.

```python
# simple.py
""" Example of a module """

x = 42

def spam():
    """ print x """
    print('x is ',x)

def run():
    """ Calls spam """
    print('Calling spam')
    spam()

print('running ...')
run()
```

```python
>>> import simple
>>> running ...
Calling spam
x is  42

>>> simple.__doc__
>>> ' Example of a module '

>>> simple.x
>>> 42

>>> simple.run()
Calling spam
x is  42
```

### Smart Error handling

Error handling is tricky in Python, because it is hard to antecipate what kind of errors may occur. An error can break the code, generating an Exception, which is then easy to catch and treat it. **However, an error can also not break the code and generate weird behavior which can be very difficult to spot.** If your code is mission critical code, use TDD to explore several error scenarios (most when user input bad format or mistypes).

In the code below we would like **to Be Defensive**, i.e., to monitor errors when the row[2] and row[3] returns bad data. For that, when this occurs it would print. However if in production I do not want to have them print, I can silence it, using a second argument `portfolio_cost('Data/portfolio.csv', silent)`

#### Key argument passing style

The second argument `errorSpec` (code below), you may spec as optional by putting a default value , in this case 'warn'.

```python
def read_portfolio(filename, errorSpec='warn'):
```

However when calling it, in order to read better for the user, you may consider forcing to write the name of key argument. For that in the function argument, you preceed the argument with \* and do not leave optional:

```python
def read_portfolio(filename, *, errorSpec):
```

Then when you call it, you need to inform explicitly the key argument and its value. Use the file "missing.csv" to test bad data with all errorSpec options.

```python
 port = read_portfolio('Data/missing.csv', errorSpec='silent')
```

Now below the full code with a defensive strategy to handle errors.

```python
import csv

def read_portfolio(filename, *, errorSpec='warn'):
    '''
    Read a CSV file with name, date, shares, price data int
    This function accepts two arguments: You should provide a filename with CSV
    And specify errorSpec with either 'warn', 'raise' or 'silent'
    So if it encounters a error, if errorSpec == 'warn' it will indicate the row and error type
    if errorSpec == 'raise', if it encounter an error it will raise in the stacktrace
    if errorSpec == 'silent', even if encounter an error, it won't print anything.
    '''
    if errorSpec not in {'warn', 'silent', 'raise'}:
        raise ValueError("errors must be one of 'warn', 'silent', 'raise'")

    portfolio = []
    with open(filename,'r') as f:
        rows = csv.reader(f)
        headers = next(rows)  # Skip first line
        for rowno, row in enumerate(rows, start=1):
            try:
                row[2] = int(row[2])
                row[3] = float(row[3])
            except ValueError as err: # will store the Exception in a variable "err"
                if errorSpec == 'warn':
                    print('Row', rowno, 'Bad row', row)
                    print('Row', rowno, 'Reason', err)
                elif errorSpec == 'raise':
                    raise  # Re raises last exception
            # record=tuple(row)
            record = {
                'name': row[0],
                'date': row[1],
                'shares': row[2],
                'price': row[3]
            }
            portfolio.append(record)
    return portfolio

    # or return a json fle (to ship in an API for example)
    import json
    return json.dumps(portfolio)

    # opposite
    json.loads(data) # transform json to python dict

```

### Tuples, lists, dicts and sets

```python
portfolio  = read_portfolio('Data/portfolio.csv')
#portfolio.sort(key=lambda holding: holding['name'])
list_comprehension = [port['shares'] * port['price'] for port in portfolio]

list_comprehension100 = [port['shares'] * port['price'] for port in portfolio if port['shares'] >= 100]
# get the unique stock names because a set eliminate duplicates
unique_names = {holding['name'] for holding in portfolio}
a = list(unique_names)
a.sort()
unique_names = a

#unique_names = ['AA', 'CAT', 'GE', 'IBM', 'MSFT']
assert unique_names == ['AA', 'CAT', 'GE', 'IBM', 'MSFT']

# library to get stock quotes - https://towardsdatascience.com/the-easiest-way-to-pull-stock-data-into-your-python-program-yfinance-82c304ae35dc
# pip install yfinance

#import yfinance as yf

#pricedata = [yf.Ticker(name).info['regularMarketPrice'] for name in unique_names]
pricedata = [72.2, 232.57, 91.72, 139.85, 280.81]
prices=dict(zip(unique_names,pricedata))
```

Other ways to define a dict:

```python
>>> a = dict(one=1, two=2, three=3)
>>> b = {'one': 1, 'two': 2, 'three': 3}
>>> c = dict(zip(['one', 'two', 'three'], [1, 2, 3]))
>>> d = dict([('two', 2), ('one', 1), ('three', 3)])
>>> e = dict({'three': 3, 'one': 1, 'two': 2})
>>> a == b == c == d == e
True
```

### Lambda Functions - Anonymous Functions

Suppose I want to sort the below dictionary by the key name:

```python
>>> portfolio = read_portfolio('Data/portfolio.csv',errorSpec='warn')
>>> for holdings in portfolio:
...     print(holdings)
...
{'name': 'AA', 'date': '2007-06-11', 'shares': 100, 'price': 32.2}
{'name': 'IBM', 'date': '2007-05-13', 'shares': 50, 'price': 91.1}
{'name': 'CAT', 'date': '2006-09-23', 'shares': 150, 'price': 83.44}
{'name': 'MSFT', 'date': '2007-05-17', 'shares': 200, 'price': 51.23}
{'name': 'GE', 'date': '2006-02-01', 'shares': 95, 'price': 40.37}
{'name': 'MSFT', 'date': '2006-10-31', 'shares': 50, 'price': 65.1}
{'name': 'IBM', 'date': '2006-07-09', 'shares': 100, 'price': 70.44}

```

I could define a function:

```python
def holding_name(holding):
       return holding['name']

>>> holding_name(portfolio[1])
'IBM'

# and use this functions as a key to the sort method
>>> portfolio.sort(key=holding_name, reverse=False)

# reverse = True is equal to sort Descending

```

The best use of anonymous functions is in the context of an argument list for a higher-order function. Using lambda function you don´t need to define the function separately, but rather on the spot.

The `lambda` syntax is just syntactic sugar: a lambda expression creates a function object just like the `def` statement.

```python
>>> a = lambda x: 10*x
>>> a(10)
100
>>> lambda x,y: x+y*y
<function <lambda> at 0x7f0df55b8a60>
>>> a=_
>>> a(2,3)
11
>>>
#back to portfolio Example
>>> portfolio.sort(key=lambda holding: holding['name'])
>>> [holding['name'] for holding in portfolio]
['AA', 'CAT', 'GE', 'IBM', 'IBM', 'MSFT', 'MSFT']
```

#### Sorting and grouping using itertools and lambda functions

```python
# First sort the list by name
port.sort(key=lambda x:x['name'])
# Then use itertools.groupby to group the list by name
for name, items in itertools.groupby(port, key=lambda x:x['name']):
    print('name: ', name)
    for it in items:
        print(it)


name:  AA
{'name': 'AA', 'date': '2007-06-11', 'shares': 100, 'price': 32.2}
name:  CAT
{'name': 'CAT', 'date': '2006-09-23', 'shares': 150, 'price': 83.44}
name:  GE
{'name': 'GE', 'date': '2006-02-01', 'shares': 95, 'price': 40.37}
name:  IBM
{'name': 'IBM', 'date': '2006-07-09', 'shares': 100, 'price': 70.44}
{'name': 'IBM', 'date': '2007-05-13', 'shares': 50, 'price': 91.1}
name:  MSFT
{'name': 'MSFT', 'date': '2007-05-17', 'shares': 200, 'price': 51.23}
{'name': 'MSFT', 'date': '2006-10-31', 'shares': 50, 'price': 65.1}
```

#### dict list_comprehension to group holdings by name

```python
by_name = { name: list(items) for name, items in itertools.groupby(port, key=lambda x:x['name'])}

by_name['MSFT']

[{'name': 'MSFT', 'date': '2007-05-17', 'shares': 200, 'price': 51.23},
 {'name': 'MSFT', 'date': '2006-10-31', 'shares': 50, 'price': 65.1}]

```

Other example:

```python
>>> DIAL_CODES = [
... (86, 'China'),
... (91, 'India'),
... (1, 'United States'),
... (62, 'Indonesia'),
... (55, 'Brazil'),
... (92, 'Pakistan'),
... (880, 'Bangladesh'),
... (234, 'Nigeria'),
... (7, 'Russia'),
... (81, 'Japan'),
... ]
>>> country_code = {country: code for code, country in DIAL_CODES}
>>> country_code
{'China': 86, 'India': 91, 'Bangladesh': 880, 'United States': 1,
'Pakistan': 92, 'Japan': 81, 'Russia': 7, 'Brazil': 55, 'Nigeria':
234, 'Indonesia': 62}
>>> {code: country.upper() for country, code in country_code.items()
... if code < 66}
{1: 'UNITED STATES', 55: 'BRAZIL', 62: 'INDONESIA', 7: 'RUSSIA'}
```

### Python Modules

When you import a module, python runs the code inside it, i.e., puts its content in memory at run time. And it is a one time invocation, python caches it.

```python
# simple.py
""" Example of a module """

x = 42

def spam():
    """ print x """
    print('x is ',x)

def run():
    """ Calls spam """
    print('Calling spam')
    spam()

print('running ...')
run()
```

Now import the module:

```python
>>> import simple
>>> running ...
Calling spam
x is  42

>>> simple.__doc__
>>> ' Example of a module '

>>> simple.x
>>> 42

>>> simple.run()
Calling spam
x is  42
```

Notice that a module keeps the variables and functions in isolation. So the `x` in the main program does not mix with the `x` in the simple module.

Noticed that by importing the module, python ran the code below the functions. If you want to avoid it, make a if statement to run, only if the module is ran in standalone:

```python
# simple.py
""" Example of a module """

x = 42

def spam():
    """ print x """
    print('x is ',x)

def run():
    """ Calls spam """
    print('Calling spam')
    spam()

if __name__ == '__main__':
    print('running ...')
    run()
```

#### Two ways of importing a module

```python
# should be in same directory, otherwise need to specify dir
import portfolio

# need to add file as prefix
portfolio.read_portfolio('Data/portfolio.csv' errorSpec='raise')
```

or

```python
# Use * to import all functions
# from portfolio import *
from portfolio import read_portfolio

# in this case you do not need the file as prefix
read_portfolio('Data/portfolio.csv' errorSpec='raise')
```

If you keep your module files in a different directory in which you are running the code, you need to tell python where to find it.

```python
>>> import sys
>>> sys.path
>>> ['/Users/rober/OneDrive/Education/Python/Advanced-Python/beazley_course',
 '/Users/rober/.pyenv/versions/3.9.7/lib/python39.zip',
 '/Users/rober/.pyenv/versions/3.9.7/lib/python3.9',
 '/Users/rober/.pyenv/versions/3.9.7/lib/python3.9/lib-dynload',
 '',
 '/Users/rober/.pyenv/versions/3.9.7/lib/python3.9/site-packages']
 >>> sys.path.append('../Modules')

```

In Python is good to write modules to be as much general purpose as possible, so you can reuse it.

Let's rewrite read_portfolio to be more general as possible, using some tricks:

```python
>>> f = open('Data/portfolio.csv','r')
>>> import csv

>>> rows = csv.reader(f)
>>> headers = next(rows)
>>> headers
['name', 'date', 'shares', 'price']

>>> row = next(rows)
>>> row
['AA', '2007-06-11', '100', '32.20']

>>> types = [str, str, int, float]

>>> for func, val in zip(types, row):
        print(func,val)
<class 'str'> AA
<class 'str'> 2007-06-11
<class 'int'> 100
<class 'float'> 32.20

>>> converted = [func(val) for func, val in zip(types, row)]
>>> converted
['AA', '2007-06-11', 100, 32.2]

>>> dict(zip(headers, converted))
{'name': 'AA', 'date': '2007-06-11', 'shares': 100, 'price': 32.2}

```

Then you can test like:

```python
In [1]: import reader

In [2]: portfolio = reader.read_csv('../Data/portfolio.csv', [str, str, int, float])

In [3]: portfolio
Out[3]:
[{'name': 'AA', 'date': '2007-06-11', 'shares': 100, 'price': 32.2},
 {'name': 'IBM', 'date': '2007-05-13', 'shares': 50, 'price': 91.1},
 {'name': 'CAT', 'date': '2006-09-23', 'shares': 150, 'price': 83.44},
 {'name': 'MSFT', 'date': '2007-05-17', 'shares': 200, 'price': 51.23},
 {'name': 'GE', 'date': '2006-02-01', 'shares': 95, 'price': 40.37},
 {'name': 'MSFT', 'date': '2006-10-31', 'shares': 50, 'price': 65.1},
 {'name': 'IBM', 'date': '2006-07-09', 'shares': 100, 'price': 70.44}]
```

And now compose another `read_portfolio`using the general purpose reader

```python
# port.py
import reader

def read_portfolio(filename, *, errorSpec='warn'):
    '''
    Read a portfolio of stocks
    '''

    return reader.read_csv(filename, [str, str, int, float], errorSpec=errorSpec)

if __name__ == "__main__":
    portfolio = read_portfolio('../Data/portfolio.csv')

    total = sum([holding['shares'] * holding['price'] for holding in portfolio])

    print('Total cost: ',total)
```

Notice the `__main__`

```python
# port.py
import reader

def read_portfolio(filename, \*, errorSpec='warn'):
'''
Read a portfolio of stocks
'''

return reader.read_csv(filename, [str, str, int, float], errorSpec=errorSpec)

if __name__ == "__main__":
    portfolio = read_portfolio('../Data/portfolio.csv')

    total = sum([holding['shares'] * holding['price'] for holding in portfolio])

    print('Total cost: ',total)

```

### Making Packages

1. Make a new directory named with your package name. Move your library of python files.

2. Make sure if there are files that import from files in same directory, you either explicitly the module (for example `import portie.reader`) or use `from . import reader`, which is known as a **Package-relative import syntax**

3. Create a empty `__init__.py` file and then you can load the function names of each module to be used in the package.

```python
# __init__.py

# Lifting symbols from submodules up a level

from .port import read_portfolio
from .reader import read_csv
```

and then you can call directly. look ate the differences:

```python
# with the lifted symbols
import portie
portie.read_portfolio('Data/portfolio.csv') # better
```

```python
# without the lifted symbols
import portie
portie.port.read_portfolio('Data/portfolio.csv') # user needs to know which submodlue this function comes from
```

### Python Data Model

Everything in Python (at runtime) is **an object** and has:

- a single _value_,
- a single _type_,
- some number of _attributes_,
- one or more _base classes_,
- a single _id_, and
- (zero or) one or more _names_ (in one or more namespaces).

```python
>>> type(1)
>>> int
>>> type([1, 2, 3])
>>> list
```

Objects have attributes.

```python
>>> True.__doc__
>>> 'bool(x) -> bool\n\nReturns True when the argument x is true, False otherwise.\nThe builtins True and False are the only two instances of the class bool.\nThe class bool is a subclass of the class int, and cannot be subclassed.'

>>> 'spam'.upper
>>> <function str.upper>

>>> callable('spam'.upper)
>>> True

>>> 'spam'.upper()
>>> 'SPAM'

>>> 'spam'.__dir__()
>>> ['__repr__',
 '__hash__',
 '__str__',
 '__getattribute__',
 '__lt__',
 '__le__',
 '__eq__',
 '__ne__',
 '__gt__',
 '__ge__',
 '__iter__',
 '__mod__',
 '__rmod__',
 '__len__',
 '__getitem__',
 '__add__',
 '__mul__',
 '__rmul__',
 '__contains__',
 '__new__',
 'encode',
 'replace',
 'split',
 'rsplit',
 'join',
 'capitalize',
 'casefold',
 'title',
 'center',
 'count',
 'expandtabs',
 'find',
 'partition',
 'index',
 'ljust',
 'lower',
 'lstrip',
 'rfind',
 'rindex',
 'rjust',
 'rstrip',
 'rpartition',
 'splitlines',
 'strip',
 'swapcase',
 'translate',
 'upper',
 'startswith',
 'endswith',
 'islower',
 'isupper',
 'istitle',
 'isspace',
 'isdecimal',
 'isdigit',
 'isnumeric',
 'isalpha',
 'isalnum',
 'isidentifier',
 'isprintable',
 'zfill',
 'format',
 'format_map',
 '__format__',
 'maketrans',
 '__sizeof__',
 '__getnewargs__',
 '__doc__',
 '__setattr__',
 '__delattr__',
 '__init__',
 '__reduce_ex__',
 '__reduce__',
 '__subclasshook__',
 '__init_subclass__',
 '__dir__',
 '__class__']
```

Objects have base classes.

```python
>>> import inspect
>>> inspect.getmro(type('spam'))
>>> (str, object)

>>> inspect.getmro(type(Card))
>>> (type, object)

>>> inspect.getmro(type(frenchDeck))
>>> (__main__.FrenchDeck, object)  # __main__. indicate a class
```

### The `class`Statement

New objects are defined using the `class` statement. A class typically consists of a collection of functions that make up the methods. It’s important to note that a class statement by itself doesn’t create any instances of the class. Rather, a class merely holds the methods that will be available on the instances created later. You might think of it as a blueprint.

A class definition may optionally include a documentation string and type hints. For example:

```python
class Account:
    '''
    A simple bank account
    '''
    owner: str
    balance: float

    def __init__(self, owner:str, balance:float):
        self.owner = owner
        self.balance = balance

    def __repr__(self):
        return f'Account({self.owner!r}, {self.balance!r})'

    def deposit(self, amount:float):
        self.balance += amount

    def withdraw(self, amount:float):
        self.balance -= amount

    def inquiry(self) -> float:
        return self.balance

```

**The functions defined inside a class are known as methods**. An instance method is a function that operates on an instance of the class, which is passed as the first argument. By convention, this argument is called **self**. In the preceding example, `deposit()`, `withdraw()`, and `inquiry()` are examples of instance methods.

The `__init__()` and `__repr__()` methods of the class are examples of so-called special or magic methods. These methods have special meaning to the interpreter runtime. The `__init__()` method is used to initialize state when a new instance is created. The `__repr__()` method returns a string for viewing an object. Defining this method is optional, but doing so simplifies debugging and makes it easier to view objects from the interactive prompt.

**Type hints** do not change any aspect of how a class works—that is, they do not introduce any extra checking or validation. It is purely metadata that might be useful for third-party tools or IDEs, or used by certain advanced programming techniques

We can implement _Special Methods_ (Magic Methods, dunder methods), so in the example below, a object instance of FrenchDeck will respond to the method `len(nameofinstanceFrenchDeck)`.

```python
import collections

Card = collections.namedtuple('Card', ['rank', 'suit'])

class FrenchDeck:
    ranks = [str(n) for n in range(2, 11)] + list('JQKA')
    suits = 'spades diamonds clubs hearts'.split()

    def __init__(self):
        self._cards = [Card(rank, suit) for suit in self.suits for rank in self.ranks]

    def __len__(self):
        return len(self._cards)

    def __getitem__(self, position):
        return self._cards[position]
```

```python
>>> frenchDeck = FrenchDeck()

>>> inspect.getmro(type(frenchDeck))
>>> (__main__.FrenchDeck, object)

>>> isinstance(frenchDeck, FrenchDeck)
>>> True

>>> frenchDeck
>>> <__main__.FrenchDeck at 0x7f4c2529d040>

>>> len(frenchDeck) # responds to .__len__()
>>> 52

>>> frenchDeck[2] # responds to .__getitem__(2)
>>> Card(rank='4', suit='spades')
```

#### How Special Methods are used?

The first thing to know about _special methods_ is that they are meant to be called by the Python interpreter, and not by you.

**You don’t write my_object.**len**()**. You write **len(my_object)** and, if my_object is an instance of a user-defined class (such as FrenchDeck), then Python calls the **len** instance method you implemented.

But for built-in types like _list, str, byte, array_, and so on, the interpreter takes a short‐cut: the CPython implementation of len() actually returns the value of the ob_size field in the PyVarObject C struct that represents any variable-sized built-in object in memory. This is much faster than calling a method.

More often than not, the special method call is implicit. For example, the statement for i in x: actually causes the invocation of iter(x), which in turn may call x.**iter**() if that is available.

Normally, your code should not have many direct calls to special methods. Unless you are doing a lot of metaprogramming, **you should be implementing special methods more often than invoking them explicitly**. The only special method that is frequently called by user code directly is **init**, to invoke the initializer of the superclass in your own **init** implementation.

If you need to invoke a special method, it is usually better to call the related built-in function (e.g., len, iter, str, etc). These built-ins call the corresponding special method, but often provide other services and for built-in types are faster than method calls.

**By implementing special methods, your objects can behave like the built-in types, enabling the expressive coding style the community considers Pythonic**.

## A Pythonic Object

Sometimes you’ll hear the word “Pythonic,” as in “this code is Pythonic.” The term is informal, but it usually refers to whether or not an object plays nicely with the rest of the Python environment. This implies supporting—to the extent that it makes sense—core Python features such as iteration, indexing, and other operations. You almost always do this by having your class implement predefined special methods.

### Understanding Attribute Access

#### Instance Attributes

Attributes and methods in python are accessible and modifiable. There are only three basic operations that can be performed on an instance: `getting`, `setting`, and `deleting` an attribute.

```python
class Holding(object):
    def __init__(self, name, date, shares, price):
        self.name = name
        self.date = date
        self.shares = shares
        self.price = price

    def cost(self):
        return self.shares * self.shares

    def sell(self, nshares):
        self.shares -= nshares
```

Even though you have defined some attributes in the `__init__` method, python allow you to set a new attribute, modify and delete. Even if you make a spelling mistake, python will add. Python does not enforce strict usage of defined attributes. There are strategies to `own the dot` to overcome problems and guarantee a safe running.

```python
>>> h = Holding('AA', '2007-12-12',100,32.2)
>>> h
<__main__.Holding object at 0x103c20d90>  # indicates an instance of Holding

# Getter atribute
>>> h.shares
100
>>> h.__getattribute__('shares') # this is what the code above calls
100

# Setter atribute
>>> h.shares= 200
>>> h
200
>>> h.__setattribute__('shares', 200)
```

#### Methods also layered in these `GET and GET` machinery

```python
>>> h.cost()
10000
>>> h.cost
<bound method Holding.cost of <__main__.Holding object at 0x103c20d90>>
>>> s = h.cost
>>> s
<bound method Holding.cost of <__main__.Holding object at 0x103c20d90>>
>>> s() # now you fire the function
10000
>>> h.__dict__
{'name': 'AA', 'date': '2007-12-12', 'shares': 100, 'price': 32.2}
>>> Holding.__dict__
mappingproxy({'__module__': '__main__', '__init__': <function Holding.__init__ at 0x103c1edc0>, 'cost': <function Holding.cost at 0x103cefdc0>, 'sell': <function Holding.sell at 0x103cefe50>, '__dict__': <attribute '__dict__' of 'Holding' objects>, '__weakref__': <attribute '__weakref__' of 'Holding' objects>, '__doc__': None})

```

`a.attr` and `getattr(a, 'attr')` are interchangeable, so `getattr(a, 'withdraw')(100)` is the same as `a.withdraw(100)`. It matters not that withdraw() is a method.

```python
>>> a = Account('Guido', 1000.0)
>>> getattr(a, 'owner')
'Guido'
>>> setattr(a, 'balance', 750.0)
>>> delattr(a, 'balance')
>>> hasattr(a, 'balance')
False
>>> getattr(a, 'withdraw')(100)    # Method call
>>> a
Account('Guido', 650.0)
>>>
```

The getattr() function is notable for taking an optional default value. If you wanted to look up an attribute that may or may not exist, you could do this:

By using `__getattr__`, `__setattr__` or `__delattr__`, you can write more general purpose code (function). Down below, you find more understanding of the usage of these methods to better control your attributes.

```python
def print_table(objects, colnames):
    '''
    Make a nicely formatted table showing attributed from a list of objects
    '''

    for colname in colnames:
        print('{:>10s}'.format(colname), end=' ')
    print()

    for obj in objects:
        for colname in colnames:
            print('{:>10s}'.format(str(getattr(obj, colname))), end=' ')
        print()
```

#### Duck Typing

`Duck Typing` is a term commonly related to dynamically typed programming languages and polymorphism. The idea behind this principle is that the code itself does not care about whether an object is a duck, but instead it does only care about whether it quacks.

#### Class Attributes

```python
class Pizza:

    # Class attributes
    diameter = 40  # cm
    slices = 8

    # Instance attributes
    def __init__(self, flavor='Cheese', flavor2=None):
        self.flavor = flavor
        self.flavor2 = flavor2
```

Good practices shown here:

- use of class attributes for attributes shared by all instances;
- attributes that are expected to vary among instances are instance attributes;
- instance attributes are all assigned in `__init__`;
- default values for instance attributes are `__init__` argument defaults.
- PEP 412 — Key-Sharing Dictionary introduced an optimization that saves memory when instances of a class have the same instance attribute names set on `__init__`.

### But still you can modify, insert, delete variables. How can you control better the attributes?

#### Some strategies:

1. typing.NamedTupple
2. @dataclass
3. Owning the dot

#### NamedTuple

```python
from typing import NamedTuple, ClassVar

class Coordinate(NamedTuple):

    lat: float = 0
    long: float = 0

    reference_system = 'WGS84'

    def __repr__(self):
        return f'Coordinate({self.lat}, {self.long})'

    def __str__(self):
        ns = 'NS'[self.lat < 0]
        we = 'EW'[self.long < 0]
        return f'{abs(self.lat):.1f}°{ns}, {abs(self.long):.1f}°{we}'
```

```python
>>> Coordinate()
Coordinate(0.0, 0.0)
>>> Coordinate.__dict__
mappingproxy({'__doc__': 'Coordinate on Earth', '__slots__': (), '_fields': ('lat', 'long'), '_field_defaults': {'lat': 0.0, 'long': 0.0}, '__new__': <staticmethod object at 0x1006cca60>, '_make': <classmethod object at 0x1006cce50>, '_replace': <function Coordinate._replace at 0x1006c6ee0>, '__repr__': <function Coordinate.__repr__ at 0x1006c6c10>, '_asdict': <function Coordinate._asdict at 0x1006d1040>, '__getnewargs__': <function Coordinate.__getnewargs__ at 0x1006d10d0>, 'lat': _tuplegetter(0, 'Alias for field number 0'), 'long': _tuplegetter(1, 'Alias for field number 1'), '__module__': '__main__', '__annotations__': {'lat': <class 'float'>, 'long': <class 'float'>}, 'reference_system': 'WGS84', '__str__': <function Coordinate.__str__ at 0x1006c6d30>, '__orig_bases__': (<function NamedTuple at 0x1006adc10>,)})

>>> for k, v in Coordinate.__dict__.items():
        if not k.startswith('_'):
            print(k, ':', v)
lat : _tuplegetter(0, 'Alias for field number 0')
long : _tuplegetter(1, 'Alias for field number 1')
reference_system : WGS84

>>> cle = Coordinate(41.40, -81.85)
>>> cle
Coordinate(41.4, -81.85)
>>> print(cle)
41.4°N, 81.8°W

>>> try:
        cle.lat = 0
    except AttributeError as e:
        print(e)
can't set attribute  # tuples are immutable -- limitation
```

#### Now using @dataclass

This function is a decorator that is used to add generated special methods to classes.

This module provides a decorator and functions for automatically adding generated special methods such as `__init__()` and `__repr__()` to user-defined classes

The dataclass() decorator examines the class to find fields. A field is defined as a class variable that has a type annotation. With two exceptions described below, nothing in dataclass() examines the type specified in the variable annotation.

The order of the fields in all of the generated methods is the order in which they appear in the class definition.

The dataclass() decorator will add various “dunder” methods to the class, described below. If any of the added methods already exist in the class, the behavior depends on the parameter, as documented below. The decorator returns the same class that it is called on; no new class is created.

Optionally "freezes" the class, that is, does not allow changing fields after initialization, to simulate an immutable object.

```python
from dataclasses import dataclass

from typing import ClassVar

@dataclass
class Coordinate:
    lat: float
    long: float = 0

    reference_system: ClassVar[str] = 'WGS84'

    def __str__(self):
        ns = 'NS'[self.lat < 0]
        we = 'EW'[self.long < 0]
        return f'{abs(self.lat):.1f}°{ns}, {abs(self.long):.1f}°{we}'

```

```python
>>> cle = Coordinate(41.40, -81.85)
>>> cle
Coordinate(41.4, -81.85)
>>> print(cle)
41.4°N, 81.8°W

>>> cle.__dataclass_fields__
{'lat': Field(name='lat',type=<class 'float'>,default=0.0,default_factory=<dataclasses._MISSING_TYPE object at 0x119f565e0>,init=True,repr=True,hash=None,compare=True,metadata=mappingproxy({}),_field_type=_FIELD),
 'long': Field(name='long',type=<class 'float'>,default=0.0,default_factory=<dataclasses._MISSING_TYPE object at 0x119f565e0>,init=True,repr=True,hash=None,compare=True,metadata=mappingproxy({}),_field_type=_FIELD),
 'reference_system': Field(name='reference_system',type=typing.ClassVar[str],default='WGS84',default_factory=<dataclasses._MISSING_TYPE object at 0x119f565e0>,init=True,repr=True,hash=None,compare=True,metadata=mappingproxy({}),_field_type=_FIELD_CLASSVAR)}

```

#### Owning the dot

Objective, avoid adding other attributes or assigning the wrong type.

1. Use of @property (getter, settter, deleters)

Here, in order to hide the `_price` attribute, a method `price` was created, however in order to assign it as a getter of `_price` we use the `@property` decorator. And then you can refer the name of these getter to defined other methods like the `setter`, `deleter`, etc.
You also avoid having to type `h.price()` with parenthesis and rather access it directly as if it was an attribute `h.price`. It is like a computed function. Take attributes and hide them behind `properties`.

Observe that now youo can take control of the attribute, enforcing a specific type, raise errors, etc.

Downside, it is `verbose`.

```python
class Holding(object):

    def __init__(self, name, date, shares, price):
        self.name = name
        self.date = date
        self.shares = shares
        self._price = price

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, newprice):
        if not isinstance(newprice, float):
            raise TypeError('Expected float')
        if newprice < 0:
            raise ValueError('Must be >0')
        self._price = newprice
```

2. Implement the methods `__getatrr__` and `__setatrr__`, so you take control of them.

```python
class Holding(object):
    """ Stock holding record: name, date, number of shares and purchased price """

    def __init__(self, name, date, shares, price):
        self.name = name
        self.date = date
        self.shares = shares
        self._price = price

    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, newprice):
        if not isinstance(newprice, float):
            raise TypeError('Expected float')
        if newprice < 0:
            raise ValueError('Must be >0')
        self._price = newprice

    def __setattr__(self, key, value):
        # need to account for _price and price
        if key not in {'name', 'date', 'shares', 'price', '_price'}:
            raise AttributeError('No attribute {}'.format(key))
        super().__setattr__(key, value)

```

### `__getattr__`

The setter checks type and the `__getattr__` checks the atributes.

2. Noticed that by magic, the implementation of `__getatrr__` provide methods of list to a class: sort, append, len, ...

```python
class Portfolio:
    def __init__(self):
        self.holdings = []

    # By implementing this getattr, the Class portfolio acquires the methods of a list: append, sort, len, etc.
    def __getattr__(self, item):
        return getattr(self.holdings, item)
```

#### Alternate Constructors - @classmethod

Here you can instantiate a class using other ways other than **init**

```python
class Date(object):
    def __init__(self, year, month, day):
        self.year = year
        self.month = month
        self.day = day

    @classmethod
    def from_string(cls, s):  # cls is "Date" self
        parts = s.split('-')
        return cls(int(parts[0]), int(parts[1]), int(parts[2]))

    @classmethod
    def today(cls):
        import time
        t = time.localtime()
        return cls(t.tm_year, t.tm_mon, t.tm_mday)
```

### Adding Special Methods to a `Portfolio` Class

When you create a class Portfolio, you loose buit-in methods, therefore it is best practice to implement them one by one (depending on your needs). In this way, you use the traditional calling syntax used in common pythonic objects (for example `len(object)`, `object[i]`, `for i in object:` - iterable, etc). You can also tweak a little bit and add customized behavior, for example see `__get_item__` method below:

```python
class Portfolio:
    def __init__(self):
        self.holdings = []

    @classmethod
    def from_csv(cls, filename):
        self = cls()  # cls is self initialized in __init__
        with open(filename, 'r') as f:
            rows = csv.reader(f)
            headers = next(rows)
            for row in rows:
                h = Holding(row[0], row[1], int(row[2]), float(row[3]))
                self.holdings.append(h)
        return self

    def __repr__(self):
        return str([p for p in self.holdings])

    def __len__(self):
        return len(self.holdings)

    def __iter__(self):
        return self.holdings.__iter__()

    def __getitem__(self, item):
        if isinstance(item, str):
            return [h for h in self.holdings if h.name == item]
        else:
            return self.holdings[item]

    def total_cost(self):
        return sum([p.shares * p.price for p in self.holdings])


```

#### Making a Custom Context Manager

This is useful when you need to manipulate stuff that need opening and closing, subscribing and unsubscribing, capture and releasing, etc.

It is the same thing when you use:

```python
with open('Data/portfolio.csv', 'r') as f:
  data = f.read()
```

for that, you also use `special methods` as follow:

```python
class Manager:

    def __enter__(self):
        return 'some value'

    def __exit__(self, ty, value, tb):
        print('Exiting')
        print(ty, val, tb)
```

The result is every time you instantiate the class and use it in the context of `with`, it will run the `__enter__` whatever in the block and at the end the `__exit__` method.

## Inheritance

Inheritance is a mechanism for creating a new class that specializes or modifies the behavior of an existing class. The original class is called a base class, superclass, or parent class. The new class is called a derived class, child class, subclass, or subtype. When a class is created via inheritance, it inherits the attributes defined by its base classes. However, a derived class may redefine any of these attributes and add new attributes of its own.

Implement when you have these ideas in mind:

- code reuse
- code extensibility (extend an existing class with new methods)

In the example below, we defined a class TableFormatter as a blueprint and later on defined specific classes that inherited TableFormatter and implemented/overrided `headings`and `row` methods. It is like defined an "abstract class" and use polimorphism implementing the specific behavior.

```python
class TableFormatter:
    """
    Serves a design specification to format a table
    """

    def headings(self, headers):
        raise NotImplementedError

    def row(self, rowdata):
        raise NotImplementedError


class TableTextFormatter(TableFormatter):
    """
    Implements a TableFormatter (by inheritance) and implements (overrides) its two methods
    """

    def headings(self, headers):
        for header in headers:
            print('{:>10s}'.format(header), end=' ')
        print()

    def row(self, rowdata):
        for item in rowdata:
            print('{:>10s}'.format(item), end=' ')
        print()


class CSVTableFormatter(TableFormatter):
    """
    Implements a TableFormatter (by inheritance) and implements (overrides) its two methods
    """

    def headings(self, headers):
        print(','.join(headers))

    def row(self, rowdata):
        print(','.join(rowdata))


class HTMLTableFormatter(TableFormatter):
    """
    Implements a TableFormatter (by inheritance) and implements (overrides) its two methods
    """

    def headings(self, headers):
        print('<tr>', end='')
        for h in headers:
            print('<th>{}</th>'.format(h), end='')
        print('</tr>')

    def row(self, rowdata):
        print('<tr>', end='')
        for d in rowdata:
            print('<td>{}</td>'.format(d), end='')
        print('</tr>')
```

Then in runtime you can use a different formatter to print a table:

```python
>>> portfolio = read_portfolio('Data/portfolio.csv')
>>> import table
>>> formatter = table.TableTextFormatter()
>>> formatter
<table.TableTextFormatter object at 0x104865bb0>
>>> table.print_table(portfolio, ['name', 'shares', 'price'], formatter)
      name     shares      price
        AA        100       32.2
       IBM         50       91.1
       CAT        150      83.44
      MSFT        200      51.23
        GE         95      40.37
      MSFT         50       65.1
       IBM        100      70.44
>>>
>>> formatter = table.CSVTableFormatter()
>>> table.print_table(portfolio, ['name', 'shares', 'price'], formatter)
name,shares,price
AA,100,32.2
IBM,50,91.1
CAT,150,83.44
MSFT,200,51.23
GE,95,40.37
MSFT,50,65.1
IBM,100,70.44
>>>
>>> formatter = table.HTMLTableFormatter()
>>> table.print_table(portfolio, ['name', 'shares', 'price'], formatter)
<tr><th>name</th><th>shares</th><th>price</th></tr>
<tr><td>AA</td><td>100</td><td>32.2</td></tr>
<tr><td>IBM</td><td>50</td><td>91.1</td></tr>
<tr><td>CAT</td><td>150</td><td>83.44</td></tr>
<tr><td>MSFT</td><td>200</td><td>51.23</td></tr>
<tr><td>GE</td><td>95</td><td>40.37</td></tr>
<tr><td>MSFT</td><td>50</td><td>65.1</td></tr>
<tr><td>IBM</td><td>100</td><td>70.44</td></tr>
>>>

```

### Attention when designing inheritance

1. Look out for avoid collision in `init` methods of Parent and Child.

```python
class TableFormatter:
    def __init__(self, outfile=None):
        if outfile is None:
            outfile = sys.stdout
        self.outfile = outfile
    ...


class TableTextFormatter(TableFormatter):
    def __init__(self, width=10):
        self.width = width
    ...
```

This code above will break, because when initialize the child TableTextFormatter, it misses the `outfile` from the parent TableFormatter.
So, you need to add all the attributes of the parent class, as well as initialize it, in order to then save and initialize child parameter.

```python
class TableTextFormatter(TableFormatter):
    def __init__(self, outfile=None, width=10): # include parent's param
        super().__init__(outfile)  # initialize parent
        self.width = width

    # Notice that now I can nest the format with two nested curly braces

    def headings(self, headers):
        for header in headers:
            print('{:>{}s}'.format(header, self.width), end=' ', file=self.outfile)
        print(file=self.outfile)

    def row(self, rowdata):
        for item in rowdata:
            print('{:>{}s}'.format(item, self.width), end=' ', file=self.outfile)
        print(file=self.outfile)
```

```python
>>> import table
>>> formatter = table.TableTextFormatter(width=20)
>>> table.print_table(portfolio, ['name', 'shares', 'price'], formatter)
```

Other example of a class that inherits a class of type TextTableFormater and twicks it for customize some behavior:

```python
class QuotedTextTableFormatter(table.TextTableFormatter):
     def row(self, rowdata):
             quoted = ['"{}"'.format(d) for d in rowdata]
             super().row(quoted)
```

output altered (inserted quotes):

```python
>>> portfolio = read_portfolio('Data/portfolio.csv')
>>> formatter = QuotedTextTableFormatter()

>>> formatter
<__main__.QuotedTextTableFormatter object at 0x104f43370>
>>> QuotedTextTableFormatter.__mro__
(<class '__main__.QuotedTextTableFormatter'>, <class 'table.TextTableFormatter'>, <class 'table.TableFormatter'>, <class 'object'>)

>>> table.print_table(portfolio, ['name', 'shares', 'price'], formatter)
      name     shares      price
      "AA"      "100"     "32.2"
     "IBM"       "50"     "91.1"
     "CAT"      "150"    "83.44"
    "MSFT"      "200"    "51.23"
      "GE"       "95"    "40.37"
    "MSFT"       "50"     "65.1"
     "IBM"      "100"    "70.44"
>>>
```

2- Using multiple inheritance to combine behavior of the parent classes. You combine like "Lego" blocks

```python
class CSVTableFormatter(TableFormatter):
    """
    Implements a TableFormatter (by inheritance) and implements (overrides) its two methods
    """

    def headings(self, headers):
        print(','.join(headers))

    def row(self, rowdata):
        print(','.join(rowdata))

class QuotedMixin:
    def row(self, rowdata):
        quoted = ['"{}"'.format(d) for d in rowdata]
        super().row(quoted)
```

output with multiple inheritance:

```python
class Formatter(table.QuotedMixin, table.CSVTableFormatter):
     pass

>>> formatter = Formatter()
>>> formatter
<__main__.Formatter object at 0x10e797c40>
>>> table.print_table(portfolio, ['name', 'shares', 'price'], formatter)
name,shares,price
"AA","100","32.2"
"IBM","50","91.1"
"CAT","150","83.44"
"MSFT","200","51.23"
"GE","95","40.37"
"MSFT","50","65.1"
"IBM","100","70.44"

# Another one, now with HTML Formatter
class Formatter(table.QuotedMixin, table.HTMLTableFormatter):
    pass
>>> formatter = Formatter()
>>> table.print_table(portfolio, ['name', 'shares', 'price'], formatter)
<tr><th>name</th><th>shares</th><th>price</th></tr>
<tr><td>"AA"</td><td>"100"</td><td>"32.2"</td></tr>
<tr><td>"IBM"</td><td>"50"</td><td>"91.1"</td></tr>
<tr><td>"CAT"</td><td>"150"</td><td>"83.44"</td></tr>
<tr><td>"MSFT"</td><td>"200"</td><td>"51.23"</td></tr>
<tr><td>"GE"</td><td>"95"</td><td>"40.37"</td></tr>
<tr><td>"MSFT"</td><td>"50"</td><td>"65.1"</td></tr>
<tr><td>"IBM"</td><td>"100"</td><td>"70.44"</td></tr>
>>>
```

2. Consider using Abstract Class to enforce class implementationon children classes:

```python
from abc import ABC, abstractmethod

class TableFormatter(ABC):
    """
    Serves a design specification to format a table
    With variation to specify output (file or print screen)
    """

    def __init__(self, outfile=None):
        if outfile is None:
            outfile = sys.stdout
        self.outfile = outfile

    @abstractmethod
    def headings(self, headers):
        pass

    @abstractmethod
    def row(self, rowdata):
        pass
```

3- Also consider using `type checking` before making calculations:

```python
def print_table(objects, colnames, formatter):

    # type checking

    if not isinstance(formatter, TableFormatter):
        raise TypeError('formatter must be a TableFormatter')

    formatter.headings(colnames)
    for obj in objects:
        rowdata = [str(getattr(obj, colname)) for colname in colnames]
        formatter.row(rowdata)
```

4- Take notice of order of multiple inheritance, that it will fire the methods accordingly.

Occasionally, a derived class would reimplement a method but also need to call the original implementation. A method can explicitly call the original method using `super()`

```python

class Parent:
     def spam(self):
             print('Parent.spam')

>>> Parent().spam()
Parent.spam

>>> Parent.__mro__
(<class '__main__.Parent'>, <class 'object'>)
>>> int.__mro__
(<class 'int'>, <class 'object'>)

class A(Parent):
     def spam(self):
             print('A.spam')
             super().spam()

>>> A().spam()
A.spam
Parent.spam

>>> A.__mro__
(<class '__main__.A'>, <class '__main__.Parent'>, <class 'object'>)

class B(A):
     def spam(self):
             print('B.spam')
             super().spam()

>>> B().spam()
B.spam
A.spam
Parent.spam
>>> B.__mro__
(<class '__main__.B'>, <class '__main__.A'>, <class '__main__.Parent'>, <class 'object'>)
>>> isinstance(B,A)
False
>>> b=B()
>>> isinstance(b,B)
True
>>> isinstance(b,A)
True
>>> isinstance(b,Parent)
True
>>> isinstance(b,object)
True
>>> isinstance(b,int)
False

class C(Parent):
    def spam(self):
        print('C.spam')
        super().spam()

class D(Parent):
    def spam(self):
        print('D.spam')
        super().spam()

>>> class E(A, C, D):
...     pass
...
>>> e = E()
>>> e.spam()
A.spam
C.spam
D.spam
Parent.spam

>>> class F(D, C, A):
...     pass
...
>>> f = F()
>>> f.spam()
D.spam
C.spam
A.spam
Parent.spam

>>> F.__mro__
(<class '__main__.F'>, <class '__main__.D'>, <class '__main__.C'>, <class '__main__.A'>, <class '__main__.Parent'>, <class 'object'>)
```

### Avoiding Inheritance via Composition

## Functions and Closures

Functions are first class citizens. We can call a function, enter another function as an argument, and the function can return another function. Function is like any other object in python model.

```python
def typed_property(name, expected_type):
    private_name = '_' + name

    @property
    def prop(self):
        return getattr(self, private_name)

    @prop.setter
    def prop(self,value):
        if not isinstance(value, expected_type):
            raise TypeError(f'Expected {expected_type}')
        setattr(self, private_name, value)

    return prop  # returns a function

Integer = lambda name: typed_property(name, int)
Float = lambda name: typed_property(name, float)
String = lambda name: typed_property(name, str)

class Holdings():
    shares = Integer('shares')
    price = Float('price')
    name = String('name')
    def __init__(self, name, date, shares, price):
        self.name = name
        self.date = date
        self.shares = shares
        self.price = price

```

Recall the `lambda` rationale.

`shares = Integer('shares')` is function, that takes 'shares' as argument and invokes another function `typed_property(name, int)`.

It is like calling: `shares = Integer(typed_property('shares', int))`

A function that takes a function as an argument or returns a function as the result is a `higher-order function`. The built-in function `sorted`: the optional key argument lets you provide a function to be applied to each item for sorting

```python
>>> fruits = ['strawberry', 'fig', 'apple', 'cherry', 'raspberry', 'banana']
>>> sorted(fruits, key=len)
['fig', 'apple', 'cherry', 'banana', 'raspberry', 'strawberry']
>>>
```

In the **functional programming paradigm**, some of the best known higher-order functions are `map`, `filter`, `reduce`, and `apply`.

## Function Argument Passing and Calling Conventions

```python
>>> def func(x, y, z):
        print(x, y, z)

>>> func(x, y, z)
1 2 3
>>> func(1, z=3, y=2)
1 2 3
```

#### Usage of `*args` - arguments

```python
>>> def func(x, *args):
        print(x)
        print(args)

>>> func(1)
1
()
>>> func(1, 2, 3, 4, 5)
1
(2, 3, 4, 5)
```

So, `*args` stores the arguments in a tuple.

#### Usage of `*kwargs` - Key arguments

```python
>>> def func(x, *kwargs):
        print(x)
        print(kwargs)

>>> func(1, xmin=10, xmax=20, color='red')
1
{'xmin':10, 'xmax':20, 'color':'red''}
>>> func(1, speed='fast')
1
{'speed':'fast'}
```

So, `*kwargs` stores the arguments in a dictionary.

#### Combination of `*args` with `*kwargs` - Accepts any combination of arguments whatsoever. Anything that is provided by key words, it will group in a dict and anything that is provided by position it will group in the tuple.

```python
>>> def func(x, *kwargs):
        print(x)
        print(kwargs)

>>> func(1, xmin=10, xmax=20, color='red')
1
{'xmin':10, 'xmax':20, 'color':'red''}
>>> func(1, speed='fast')
1
{'speed':'fast'}
```

So, `*kwargs` stores the arguments in a dictionary.

## Iterators & Generators

Every standard collection in Python is iterable. An iterable is an object that provides an `iterator`, which Python uses to support operations like:

- for loops
- List, dict, and set comprehensions
- Unpacking assignments
- Construction of collection instances

An iterator is an object representing a stream of data; this object returns the data one element at a time. A Python iterator must support a method called `__next__()` that takes no arguments and always returns the next element of the stream. If there are no more elements in the stream, `__next__()` must raise the **StopIteration** exception. Iterators don’t have to be finite, though; it’s perfectly reasonable to write an iterator that produces an infinite stream of data.

If the data doesn’t fit in memory, we need to fetch the items lazily—one at a time and on demand. That’s what an iterator does.

The built-in `iter()` function takes an arbitrary object and tries to return an iterator that will return the object’s contents or elements, raising TypeError if the object doesn’t support iteration. Several of Python’s built-in data types support iteration, the most common being lists and dictionaries. An object is called iterable if you can get an iterator for it.

```python
>>> L = [1, 2, 3]
>>> it = iter(L)
>>> it
<...iterator object at ...>
>>> it.__next__()  # same as next(it)
1
>>> next(it)
2
>>> next(it)
3
>>> next(it)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
>>>
```

`Generators` are often presented as a convenient way to define new kinds of iteration patterns.

Generators are a special class of functions that simplify the task of writing iterators. Regular functions compute a value and return it, but generators return an iterator that returns a stream of values.

If a function uses the `yield` keyword, it defines an object known as a `generator`. **The primary use of a generator is to produce values for use in iteration**.

```python
def countdowny(n):
     print('Counting down from', n)
     while n > 0:
             yield n
             n -= 1

>>> type(countdowny)
<class 'function'>
>>> countdowny(5)  # it does not execute the function, rather it created a generator object
<generator object countdowny at 0x7f7af59659e0>
>>> type(countdowny(5))
<class 'generator'>

# Now You can use that object to iterate
>>> for x in countdowny(5):
...     print('T-minus', x)
...
Counting down from 5
T-minus 5
T-minus 4
T-minus 3
T-minus 2
T-minus 1

# Or you can consume a generator one-by-one
>>> c = countdowny(5)
>>> next(c)
Counting down from 5
5
>>> next(c)
4
>>> next(c)
3
>>> next(c)
2
>>> next(c)
1
>>> next(c)
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
StopIteration
```

The last example using `next(c)`, once the iteration reaches the end, it `StopIteration` and then the generator is no longer usable. You have to create another one.

Generator functions are one of Python’s most interesting and powerful features. Generators are often presented as a convenient way to define new kinds of iteration patterns. However, there is much more to them: Generators can also fundamentally change the whole execution model of functions.

A generator function produces items until it returns—by reaching the end of the function or by using a return statement. This raises a StopIteration exception that terminates a for loop. If a generator function returns a non-None value, it is attached to the StopIteration exception. For example, this generator function uses both yield and return:

```python
def func():
yield 37
return 42
```

Here’s how the code would execute:

```python

f = func()
f
<generator object func at 0x10b7cd480>
next(f)
37
next(f)
Traceback (most recent call last):
File "<stdin>", line 1, in <module>
StopIteration: 42

```

Observe that the return value is attached to StopIteration.

To collect this value, you need to explicitly catch StopIteration and extract the value.

If you want an object that allows repeated iteration, define it as a class and make the **iter**() method a generator:

```python
class countdown:
    def __init__(self, start):
        self.start = start

    def __iter__(self):
        n = self.start
        while n > 0:
            yield n
            n -= 1

```

This works because each time you iterate, a fresh generator is created by `__iter__()`.

The `map` and `filter` functions are still built-ins in Python 3, but since the introduction of `list comprehensions` and `generator expressions`, they are not as important. A listcomp or a genexp does the job of map and filter combined, but is more readable.

```python
>>> list(map(factorial, range(6)))  # Build a list of factorials from 0! to 5!
[1, 1, 2, 6, 24, 120]
>>> [factorial(n) for n in range(6)]  # Same operation, with a list comprehension
[1, 1, 2, 6, 24, 120]
>>> list(map(factorial, filter(lambda n: n % 2, range(6))))  # List of factorials of odd numbers up to 5!, using both map and filter
[1, 6, 120]
>>> [factorial(n) for n in range(6) if n % 2]  # List comprehension does the same job, replacing map and filter, and making lambda unnecessary
[1, 6, 120]
>>>
```

Generator expressions are surrounded by parentheses (“()”) and list comprehensions are surrounded by square brackets (“[]”). Generator expressions have the form:

```python
( expression for expr in sequence1
             if condition1
             for expr2 in sequence2
             if condition2
             for expr3 in sequence3
             ...
             if condition3
             for exprN in sequenceN
             if conditionN )
```

a list comprehension or generator expression is equivalent to the following Python code:

```python
for expr1 in sequence1:
    if not (condition1):
        continue   # Skip this element
    for expr2 in sequence2:
        if not (condition2):
            continue   # Skip this element
        ...
        for exprN in sequenceN:
            if not (conditionN):
                continue   # Skip this element

            # Output the value of
            # the expression.
```

## Metaclasses

## Decorators and Closures

Function decorators let us “mark” functions in the source code to enhance their behavior in some way. This is powerful stuff, but mastering it requires understanding `closures`— which is what we get when functions capture variables defined outside of their bodies.

Let' say we have the following function:

```python
>>>def add(x,y):
       return (x+y)
>>> add(6,3)
>>> 9
```

and we want to add a new functionality withou touching the original code. One way is to make a wrapper function that takes the function `add` as an argument, add the new functionality and return it back.

```python
def logged(func):
    # Idea: give me a function, I'll put logging around it

    print('Adding logging to ', func.__name__)

    def wrapper(*args, **kwargs):
        print('Calling', func.__name__)
        return func(*args, **kwargs)
    return wrapper
```

Noticed that the wrapper function is a `closure`, because it is taking the variable `func` which is defined out of its scope, in fact, `func` is defined in the function `logged`.

Actually, a **closure** is a function, in this example `wrapper` with an extended scope that encompasses variables referenced in the body of `wrapper` that are not global variables or local variables of `wrapper`. Such variables must come from the local scope of an outer function, i.e. `logged` that encompasses `wrapper`.

See that we can now wrap `logged` around `add` to redefine `add` with `logged` functionality.

```python
>>> add = logged(add)
>>> add
<function __main__.logged.<locals>.wrapper(*args, **kwargs)>
>>> add(3,6)
Calling add
>>> 9
```

This idea of taking a function and putting a wrapper layer around it adding a new feature is what defines a `decorator`.

#### Ways to implement a decorator

```python
def add(x,y):
    return (x+y)
add = logged(add)
```

or

```python
@logged
def add(x,y):
    return (x+y)
```

Notice that the name logged and wrapper could be any name. I could have defined the same code with:

```python
def deco(func):
    # Idea: give me a function, I'll put logging around it

    print('Adding logging to ', func.__name__)

    def inner(*args, **kwargs):
        print('Calling', func.__name__)
        return func(*args, **kwargs)
    return inner

>>> add = deco(add)

# or
@deco
def add(x,y):
    return (x+y)

>>> add
<function deco.<locals>.inner at 0x10063b598>
```

Three essential facts make a good summary of decorators:

- A decorator is a function or another callable.
- A decorator may replace the decorated function with a different one.
- Decorators are executed immediately when a module is loaded.

A key feature of decorators is that they run right after the decorated function is defined. That is usually at import time (i.e., when a module is loaded by Python).

In order to the wrapper function behaves similarly to the wrapped function, we need to either mannually

```python
def deco(func):
    def inner(*args, **kwargs):
        print('Calling', func.__name__)
        return func(*args, **kwargs)

        # Mannually assignment
        inner.__name__ = func.__name__
        inner.__doc__ = func.__doc__
    return inner
```

or usinf `@wraps` decorator from functools:

```python
from functools import wraps
def deco(func):

    @wraps(func)
    def inner(*args, **kwargs):
        print('Calling', func.__name__)
        return func(*args, **kwargs)

    return inner
```

Decorator is a kind of `Metaprogramming`, which is a programming technique in which computer programs have the ability to treat other programs as their data. This means that a program can be designed to read, generate, analyze, or transform other programs, and even modify itself while running.

Just like `metadata is data about data`, `metaprogramming is writing programs that manipulate programs`.

## Coroutines

Coroutines are computer program components that generalize subroutines for non-preemptive multitasking, by allowing execution to be suspended and resumed. Coroutines are well-suited for implementing familiar program components such as cooperative tasks, exceptions, event loops, iterators, infinite lists and pipes.

Essentially what a co-routine is, is it's a function that has to operate under the management, of some other piece of code. It turns out that this function will not execute unless something else runs it.

Now, one of the things that might run this code, is a standard library in Python, such as the `asyncio` library. What you do, is you ask for `an event loop`, and then there's a method on there, where you can run something until it's complete. What's happening here, is, I've got this async function that has been defined. You call it, but some other piece of code, has to run the function. Think of it as a function, that is running under management though. Something is managing that code.

```python
async def greeting(name):
    return f"Hello {name}"

>>> g
<coroutine object greeting at 0x10e84f240>
# The function did not run
# You need to use asyncio and launch a event loop that will manage and execute async code.
>>> import asyncio
>>> loop = asyncio.get_event_loop()
>>> loop
<_UnixSelectorEventLoop running=False closed=False debug=False>
>>> loop.run_until_complete(g)
'Hello Guido'

async def Hello():
...     names=['Guido', 'Luigi', 'Mario']
...     for name in names:
...             print(await greeting(name))
...
>>> h=Hello()
>>> h
<coroutine object Hello at 0x10f025240>
>>> loop.run_until_complete(h)
Hello Guido
Hello Luigi
Hello Mario
```

## LaTex

-$x + y$

$\pi \approx 3.14159$

$\large \theta_0 \normalsize =  x^{1}_{[0]}$
